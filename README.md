This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Converts NUnit3 xml reports to an html-file
* version 0.1. 
* I can't really recommend this one, use with care :)

###Screen shots:###
![Screen Shot 2016-11-28 at 22.28.17.png](https://bitbucket.org/repo/x4x6B9/images/748008468-Screen%20Shot%202016-11-28%20at%2022.28.17.png)
![Screen Shot 2016-11-28 at 22.05.53.png](https://bitbucket.org/repo/x4x6B9/images/2754298266-Screen%20Shot%202016-11-28%20at%2022.05.53.png)

Display overview of the test run (total test cases, passed, failed+error, ignored)

Also display all test cases of a test fixture, colored due to result and display time it took

Expand a test case to see its output (if success) or message+stack trace (if fail or error).

### How do I get set up? ###

* Usage: 
* Build to create the .exe file
* path/to/NUnit2HTML.exe path/to/nunit3testReport.xml path/for/output.html
 (one day I will make this a NuGet package...)

### Contribution guidelines ###

* TBD

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact