﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace NUnit2HTML
{
    public class TestCase
    {
        private string _duration;
        private string _output;
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Name { get; set; }
        public string Result { get; set; }
        public string Label { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Duration
        {
            get
            {
                var durationAsDouble = double.Parse(_duration, CultureInfo.InvariantCulture);
                var timeSpan = TimeSpan.FromSeconds(durationAsDouble);
                return timeSpan.Minutes + "min " + timeSpan.Seconds + "s";
            }

            set { _duration = value; }
        }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Output
        {
            get { return _output; }
            set
            {
                _output = new Regex("<table>").Replace(value, "table");
            }
        }
    }
}
