﻿using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using System;
using System.IO;

namespace NUnit2HTML
{
    public class PageBuilder
   {
      public string PageSkeleton(TestAssembly suite)
      {
	      var config = new TemplateServiceConfiguration {Language = Language.CSharp};
	      //config.Debug = true;

         var service = RazorEngineService.Create(config);

         Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
         var template = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/MainPage.cshtml");

         return service.RunCompile(template, "templateKey", typeof(TestAssembly), suite);
         //--- For debug purposes below:
         //string templateFile = AppDomain.CurrentDomain.BaseDirectory + "/MainPage.cshtml";
         //return service.RunCompile(new LoadedTemplateSource(template, templateFile), "templateKey", typeof(TestAssembly), suite);
      }
   }
}
