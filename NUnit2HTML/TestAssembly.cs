﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NUnit2HTML
{
	public class TestAssembly
	{
		private string _name;
		public string Name
		{
			get
			{
				return Regex.Replace(_name, ".dll", "");
			}
			set { _name = value; }
		}
        public IList<TestFixture> TestFixtures { get; set; }
		public int TestCases { get; set; }
		public int PassedTests { get; set; }
		public int FailedTests { get; set; }
		public int ErrorTests { get; set; }
		public int IgnoredTests { get; set; }
		public int InconclusiveTests { get; set; }
		public decimal SuccessRate { get; set; }
		public int Asserts { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
	}
}
