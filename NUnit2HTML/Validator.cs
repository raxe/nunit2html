﻿using System;
using System.IO;

namespace NUnit2HTML
{
    internal class Validator
    {
        internal bool FileExist(string file)
        {
            var exist = false;

            if (File.Exists(file))
            {
                Console.WriteLine("File exists, let's continue");
                exist = true;
            }
            else
            {
                Console.WriteLine("File does not exist");
            }
            return exist;
        }
    }
}
