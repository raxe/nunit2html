﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace NUnit2HTML
{
    public class ParseResultXML
    {
        private readonly string _file;
        internal ParseResultXML(string file)
        {
            this._file = file;
        }

        public TestAssembly ParsedTestSuite()
        {
            var text = XElement.Load(_file);

            var parsedTestSuite = text.Descendants("test-suite").First(x => x.Attribute("type").Value == "Assembly");

            var testSuite = new TestAssembly()
            {
                Name = parsedTestSuite.Attribute("name").Value,
                TestCases = int.Parse(parsedTestSuite.Attribute("testcasecount").Value),
                PassedTests = int.Parse(parsedTestSuite.Attribute("passed").Value),
                FailedTests = int.Parse(parsedTestSuite.Attribute("failed").Value),
                ErrorTests = 0,
                IgnoredTests = int.Parse(parsedTestSuite.Attribute("skipped").Value),
                InconclusiveTests = int.Parse(parsedTestSuite.Attribute("inconclusive").Value),
                Asserts = int.Parse(parsedTestSuite.Attribute("asserts").Value),
                StartTime = DateTime.Parse(parsedTestSuite.Attribute("start-time").Value),
                EndTime = DateTime.Parse(parsedTestSuite.Attribute("end-time").Value)
            };
            testSuite.SuccessRate = decimal.Round(decimal.Divide(testSuite.FailedTests, testSuite.TestCases - testSuite.InconclusiveTests - testSuite.IgnoredTests) * 100, 1);
            testSuite.TestFixtures = ParseTestFixtures(testSuite);
            return testSuite;
        }
        public List<TestFixture> ParseTestFixtures(TestAssembly suite)
        {
            var text = XElement.Load(_file);
            var TestFixtures = new List<TestFixture>();

            var parsedTestFixtures = text.Descendants("test-suite").Where(x => x.Attribute("type").Value == "TestFixture");

            foreach (var testFixture in parsedTestFixtures)
            {
                TestFixtures.Add(new TestFixture()
                {
                    Id = testFixture.Attribute("id").Value,
                    Name = testFixture.Attribute("name").Value,
                    TestCaseCount = int.Parse(!string.IsNullOrEmpty(testFixture.Attribute("testcasecount").Value) ? testFixture.Attribute("testcasecount").Value : "0"),
                    ChildTestCasesRaw = testFixture.Descendants("test-case"),
                    StartTime = testFixture.Attribute("start-time").Value,
                    EndTime = testFixture.Attribute("end-time").Value,
                    Duration = testFixture.Attribute("duration").Value,
                    Result = testFixture.Attribute("result").Value,
                    Passed = int.Parse(!string.IsNullOrEmpty(testFixture.Attribute("passed").Value) ? testFixture.Attribute("passed").Value : "0"),
                    Failed = int.Parse(!string.IsNullOrEmpty(testFixture.Attribute("failed").Value) ? testFixture.Attribute("failed").Value : "0"),
                    Errors = 0,
                    Inconclusive = int.Parse(!string.IsNullOrEmpty(testFixture.Attribute("inconclusive").Value) ? testFixture.Attribute("inconclusive").Value : "0"),
                    Skipped = int.Parse(!string.IsNullOrEmpty(testFixture.Attribute("skipped").Value) ? testFixture.Attribute("skipped").Value : "0"),
                    Asserts = int.Parse(!string.IsNullOrEmpty(testFixture.Attribute("asserts").Value) ? testFixture.Attribute("asserts").Value : "0"),
                });

                var currentFixture = TestFixtures.Count - 1;

                //Using SpecFlow auto adds description used to Fixture name, else using the name of the Fixture...
                if (testFixture.Element("properties") != null)
                {
                    if (testFixture.Element("properties").Element("property").Attribute("value") != null)
                        TestFixtures[currentFixture].Description = testFixture.Element("properties").Element("property").Attribute("value").Value;
                }
                else
                {
                    TestFixtures[currentFixture].Description = testFixture.Attribute("name").Value;
                }
                TestFixtures[currentFixture].ChildTestCases = ParseTestCases(suite, TestFixtures[currentFixture]);
            }
            return TestFixtures;
        }

        public List<TestCase> ParseTestCases(TestAssembly suite, TestFixture fixture)
        {
            // XElement text = XElement.Load(file);
            List<TestCase> TestCases = new List<TestCase>();

            foreach (var item in fixture.ChildTestCasesRaw)
            {
                TestCases.Add(new TestCase()
                {
                    Id = item.Attribute("id").Value,
                    ParentId = fixture.Id,
                    Name = item.Attribute("name").Value,
                    Result = item.Attribute("result").Value,
                    StartTime = item.Attribute("start-time").Value,
                    EndTime = item.Attribute("end-time").Value,
                    Duration = item.Attribute("duration").Value,
                });
                if (item.Element("output") != null)
                    TestCases[TestCases.Count - 1].Output = item.Element("output").Value;

                var testCase = TestCases[TestCases.Count - 1];

                if (testCase.Result.ToLower() == "failed")
                    testCase.Message = item.Element("failure").Element("message").Value;

                if (testCase.Result.ToLower() == "failed")
                {
	                var xElement = item.Element("failure");
	                var element = xElement?.Element("stack-trace");
	                if (element != null)
		                testCase.StackTrace = element.Value;
                }

	            if (item.Attribute("label") != null && testCase.Result != "Skipped")
                {
                    testCase.Result = item.Attribute("label").Value;
                    suite.FailedTests -= 1;
                    suite.ErrorTests += 1;
                    fixture.Failed -= 1;
                    fixture.Errors += 1;
                }

            }

            return TestCases;
        }
    }
}
