﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace NUnit2HTML
{
	public class Program
	{
		#region starters
		private const string Usage = "Usage: NUnit2HTML.exe path/to/file.xml path/to/generatedReport.html";

		private static readonly Regex Regex = new Regex("[^a-zA-Z0-9 -]");

		private static readonly List<string> HelpParameters = new List<string>() { "?", "/?", "help" };
		#endregion
		public static void Main(string[] args)
		{
			//string[] args = new string[2];
			//args[0] = @"Y:\Desktop\git\TestRapport.xml";
			//args[1] = @"Y:\Desktop\git\TestRapport.html";
			//args[0] = @"C:\Users\piersteh456\Videos\TR_Market_BDD.xml";
			//args[1] = @"C:\Users\piersteh456\Videos\TR_Market_BDD.html";


			var html = "";
			var validator = new Validator();
			var fileOk = false;
			var input = string.Empty;
			var output = string.Empty;

			if (args.Length == 1)
			{
				input = args[0];
				Console.WriteLine("File used: {0}", input);
				// Check if the user wants help, otherwise assume its a
				// filename that needs to be processed
				if (HelpParameters.Contains(input))
				{
					Console.WriteLine(Usage);
				}
				else
				{
					// Output file with the same name in the same folder
					// with a html extension
					output = Path.ChangeExtension(input, "html");

					// Check input file exists and output file doesn't
					fileOk = validator.FileExist(input);
				}
			}
			else if (args.Length == 2)
			{
				// If two parameters are passed, assume the first is 
				// the input path and the second the output path
				input = args[0];
				output = args[1];

				Console.WriteLine("File used: {0} and output is: {1}", input, output);

				// Check input file exists and output file doesn't

				fileOk = validator.FileExist(input);
			}
			else
			{
				// Display the usage message
				Console.WriteLine(Usage);
			}

			// If input file exists and output doesn't exist
			if (fileOk)
			{
				Console.WriteLine("File OK, start processing test report.");
				//Parse xml file for data and fill the objects with it
				var testSuite = new ParseResultXML(input).ParsedTestSuite();

				Console.WriteLine("Test report processed. Starting build HTML file.");
				// Generate the HTML page
				html = new PageBuilder().PageSkeleton(testSuite);

				Console.WriteLine("HTML file built, starting generate and public result.");
				// Save HTML to the output file
				File.WriteAllText(output, html);
			}
		}
	}
}

