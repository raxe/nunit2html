﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;

namespace NUnit2HTML
{
    public class TestFixture
    {
        private string _duration;
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TestCaseCount { get; set; }
        public IEnumerable<XElement> ChildTestCasesRaw { get; set; }
        public List<TestCase> ChildTestCases { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Duration
        {
            get
            {
                var durationAsDouble = double.Parse(_duration, CultureInfo.InvariantCulture);
                var timeSpan = TimeSpan.FromSeconds(durationAsDouble);
                return timeSpan.Minutes+"min "+timeSpan.Seconds+"s";
            }

            set { _duration = value; }
        }
        public string Result { get; set; }
        public int Passed { get; set; }
        public int Failed { get; set; }
        public int Errors { get; set; }
        public int Inconclusive { get; set; }
        public int Skipped { get; set; }
        public int Asserts { get; set; }
    }
}
